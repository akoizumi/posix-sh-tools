include config.mk

install: install-prog install-man
uninstall: uninstall-prog uninstall-man

install-prog:
	for i in ${PROG}; do install -m0755 bin/$$i $(DESTDIR)$(PREFIX)/bin/$$i; done
install-man:
	for m in ${MAN1}; do mandoc -T man < man/$$m > $(DESTDIR)$(PREFIX)/share/man/man1/$$m; done
	for n in ${MAN7}; do mandoc -T man < man/$$n > $(DESTDIR)$(PREFIX)/share/man/man7/$$n; done
uninstall-prog:
	for i in ${PROG}; do rm -f $(DESTDIR)$(PREFIX)/bin/$$i; done
uninstall-man:
	for m in ${MAN1}; do rm -f $(DESTDIR)$(PREFIX)/share/man/man1/$$m; done
	for n in ${MAN7}; do rm -f $(DESTDIR)$(PREFIX)/share/man/man7/$$n; done
