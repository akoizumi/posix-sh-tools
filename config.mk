PREFIX ?= /usr/local
DESTDIR ?=

MAN1 = dns.1 \
      ipcheck.1 \
      linx.1 \
      list-fonts.1 \
      minpb.1 \
      pastor.1 \
      pbin.1 \
      rman.1 \
      tb.1 \
      tmux-wm.1 \
      weather.1

MAN7 = posix-sh-tools.7

PROG = dns \
       git-send-paste \
       ipcheck \
       linx \
       list-fonts \
       minpb \
       pastor \
       pbin \
       rman \
       tb \
       tmux-wm \
       weather
